import re
from collections import defaultdict
import argparse
from pprint import pprint

myParams = {}

def getParameters ():
	"""
	Use Argparse to setup/get options from cmd line
	"""
	# initialiser l'objet argparse
	parser = argparse.ArgumentParser()

	# ajouter autant d'options qu'on veut 
	parser.add_argument("-fa", "--fasta", help="Fasta file to convert", required=True)
	parser.add_argument("-m", "--meme", help="MEME.txt results file to convert", required=True)
	parser.add_argument("-dp", "--DrawProt", help="output Drawproteins tsv fiLe", required=True)
	parser.add_argument("-r", "--rep", help="occurences to find in fasta file", required=True)
	

	# examination de la ligne de commande saisie par le user
	# si aucun parametre: message d'erreur
	# si -h: affichage de l'aide
	# sinon: recuperation des valeurs indique 
	myParams=parser.parse_args()

	# renvoyer le dictionnaire de parametres
	return myParams

# recupere
myParams = getParameters()
pprint(myParams)


f1=open(myParams.fasta,"r")
ID=''
dicseq={}
for li in f1:
	li=li.rstrip()
	mID=re.search('^>(.+)',li)
	mSEQ=re.search('(^[A-Z]+)',li)	
	if mID:
		SEQ=""
		ID=mID.group(1)
	if mSEQ:
		SEQ+=mSEQ.group(1)
		dicseq[ID]=SEQ

#Fonction qui permet de trouver les positions d'une occurence de plusieurs lettres dans une chaine de caractères, ici FG
		
def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) 	
        
        
f2=open(myParams.DrawProt,'w')
f2.write('type'+"\t"+'description'+"\t"+'begin'+"\t"+'end'+"\t"+'length'+"\t"+'entryName'+"\t"+'order'+"\n")

dicID={}     	
dicfg=defaultdict(list)
diclen={}
b=0
for ID in dicseq:
	b+=1
	n=len(dicseq[ID])
	dicfg[ID]=(list(find_all(dicseq[ID], myParams.rep))) #On recherche les positions des répétitions FG dans toutes les séquences
	begin=0
	dicID[ID]=b
	f2.write('CHAIN'+'\t'+"Protein"+"\t"+str(begin)+"\t"+str(n)+"\t"+str(n)+"\t"+ID+"\t"+str(b)+"\n")

c=0	
for ID in dicfg:
	c+=1
	for pos in dicfg[ID]:
			end=int(pos)+1
			length=2
			f2.write('REPEAT'+"\t"+myParams.rep+"\t"+str(pos)+"\t"+str(end)+"\t"+str(length)+"\t"+ID+"\t"+str(dicID[ID])+"\n")
	

	
#Recapitulatif des resultats MEME
a=0
start=0
end=0
width=0                                      
f3=open(myParams.meme,'r')
for li in f3:
    li=li.rstrip()
    msequence=re.search('^PRIMARY SEQUENCES=.+',li)
    if msequence:
        lid=li.split('=')
        lid=lid[-1].split('.')
        fichier=lid[0] 
        f4=open('results_'+fichier+'.tsv','w')
        f4.write('motif'+'\t'+'proteins'+'\t'+'start'+'\t'+'end'+'\t'+'width'+'\t'+'e-value'+'\t'+'sequence'+'\n')
    mmotif=re.search('.+MEME-[0-9].+width =\s+(\d+).+E-value =\s+(\S+)',li)
    if mmotif:
        a+=1
        width=int(mmotif.group(1))
        evalue=float(mmotif.group(2)) 
    mstart=re.search('^(\S+)\s+([0-9]+)\s+\S+\s+([A-Z]+)',li)   
    if mstart:
        start=int(mstart.group(2)) 
        #print(start)
        proteins=mstart.group(1)
        #print(proteins)
        end=start+width
        sequence=mstart.group(3)
        f4.write(str(a)+'\t'+proteins+'\t'+str(start)+'\t'+str(end)+'\t'+str(width)+'\t'+str(evalue)+'\t'+sequence+'\n')
        f2.write('MOTIF'+'\t'+'M'+str(a)+'\t'+str(start)+'\t'+str(end)+'\t'+str(width)+'\t'+proteins+'\t'+str(dicID[proteins])+'\n')
f1.close()
f2.close()	
f3.close()
f4.close()
	
print("tsv file for DrawProteins is complete")
	
